import './App.css';
import React, { Component } from "react";
import axios from 'axios';

class App extends Component {
   
  constructor(props){
      super(props);
  }
  
  state = {
   name: '',
   email:'',
   id: '',
   searchValue: '',
   data: []
  }


  componentDidMount(){
   this.loadEmployee();
  }

  loadEmployee = ()=>{
    const self = this;
    axios.get('http://localhost:8000/employee_show').then(response => {
      self.setState({
       data: response.data,
       name: '',
       email: '',
       id: ''
      })
    })
  }

   addUser = ()=>{
    const self = this;
    const payload = {
      name: self.state.name,
      email: self.state.email,
    }

     if(self.state.id){
        axios.post(`http://localhost:8000/employee_update/${self.state.id}`, payload).then(response=>{
          alert('User has been updated successfully');
          self.loadEmployee();
        })
     }else{
        axios.post('http://localhost:8000/employee_add', payload).then(response=>{
          alert('User has been added successfully');
          self.loadEmployee();
        })
     }  
     
  }

  handleName = (e)=>{
    this.setState({name: e.target.value});
  }

  handleEmail = (e)=>{
    this.setState({email: e.target.value});
  }

  deleteUser = (id)=>{
    const self = this;
    axios.get(`http://localhost:8000/employee_delete/${id}`).then(response=>{
    alert('User has been deleted successfully');  
    self.loadEmployee();
    })
  }

  editUser = (id)=>{
    const self = this;
    axios.get(`http://localhost:8000/employee_edit/${id}`).then(response=>{
      self.setState({name: response.data.name, email: response.data.email, id: id});
    })
  }

  filter = () =>{
    const self = this;
    axios.post('http://localhost:8000/employee_search', {searchQuery: self.state.searchValue}).then(response=>{
      self.setState({data : response.data});
    })
  }

  handleSearch = (event)=>{
    this.setState({searchValue: event.target.value});
    if(!event.target.value.length){
       this.loadEmployee();
     }
  }

  render(){
    return (
      <div style={{margin: 'auto', width: '50%'}} >
      <h1  className="primaryHeading" >Add User</h1>
      <div className="textAreaDiv">
      <input type="text" id="name" placeholder="Enter name" onChange={this.handleName} value={this.state.name}/>
      </div>
      <div className="textAreaDiv">
      <input type="email" id="email" placeholder="Enter email" onChange={this.handleEmail} value={this.state.email}/>
      </div>
      <input type="Submit" className="submitBtn" onClick={this.addUser}/>
      
      <div style={{float: 'left', width: '75%', margin: '20px 0px 5px'}}><input type="text" onChange={this.handleSearch}/></div>    
      <div style={{float: 'left', width: '25%', margin: '20px 0px 5px'}}><input type="button" className="submitBtn" value="Search" onClick={this.filter}/></div>    

      <table id="user_table" style={{marginTop: '20px'}}>
        <tr><th>Id</th> <th>Name</th> <th>Email</th> <th>Action</th></tr>
        {this.state.data.map((e, index)=> {
          return <tr><td>{index+1}</td> <td>{e.name}</td> <td>{e.email}</td> <td><a href="#" onClick={()=>this.deleteUser(e.id)}>Delete</a> <a href="#" onClick={()=>this.editUser(e.id)}>Edit</a></td></tr>
        })}
      </table>
      </div>
    );
  }
}

export default App;
